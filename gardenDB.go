package main

import (
	"fmt"
)

func dbGardenCreate(newGarden apiGardenCreate) apiGardenCreateResult {
	var g Garden
	var Owner User
	var result apiGardenCreateResult

	dbOrm.Where("username = ?").First(&Owner)

	gardenNotExisting := dbOrm.Where("ref = ? AND name = ?", Owner.ID, newGarden.Name).First(&g).RecordNotFound()

	if !gardenNotExisting {
		result.Code = -1
		result.Message = "Name already exists!"
		return result
	}

	g.Name = newGarden.Name

	Owner.Gardens = append(Owner.Gardens, g)

	dbOrm.Save(&Owner)

	result.apiGardenPublic = g.apiGardenPublic

	fmt.Println("Garden created", g)
	return result
}

func dbPlantsGetAll() []Plant {

	var list []Plant
	dbOrm.Find(&list)
	return list
}
