package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{"Index", "GET", "/", Index},
	Route{"createUser", "POST", "/api/v1/createUser", CreateUser},
	Route{"UserCreateToken", "POST", "/api/v1/createToken", UserCreateToken},
	Route{"printAllUsers", "GET", "/api/v1/users", ListUsers},

	Route{"createGarden", "POST", "/api/v1/garden/create", GardenCreate},

	Route{"showPlants", "GET", "/api/v1/plants/", PlantsList},
}
