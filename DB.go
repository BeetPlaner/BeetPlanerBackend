package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"

	"golang.org/x/crypto/bcrypt"
)

var currentID int

var todos Todos
var users Users

var dbOrm *gorm.DB
var err error

// Give us some seed data
func DBInit() {

	dbOrm, err = gorm.Open("sqlite3", "planner.db")
	if err != nil {
		panic("failed to connect database")
	}

	// Migrate the schema
	dbOrm.AutoMigrate(&Todo{})
	dbOrm.AutoMigrate(&APIToken{})
	dbOrm.AutoMigrate(&User{})
	dbOrm.AutoMigrate(&Plant{})
	dbOrm.AutoMigrate(&Planting{})
	dbOrm.AutoMigrate(&Event{})
	dbOrm.AutoMigrate(&Coordinates{})
	dbOrm.AutoMigrate(&Patch{})
	dbOrm.AutoMigrate(&Garden{})

	log.Print("init Done.")

}

func dbUserCreate(newUser CreateUserData) CreateUserResult {
	var u User
	var result CreateUserResult

	if dbUserNameExists(newUser.Username) {
		fmt.Println("User already Exists")
		result.Result = -1
		result.Message = "User already Exists"
		return result
	}

	if dbMailaddressExists(newUser.Mail) {
		fmt.Println("Mail already Exists")
		result.Result = -2
		result.Message = "Mail already Exists"
		return result
	}

	fmt.Println("Userpass:", newUser)

	hash, err := bcrypt.GenerateFromPassword([]byte(newUser.ClearPWD), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
	}

	u.CreateUserData = newUser
	u.ClearPWD = ""
	u.Password = hash

	dbOrm.Create(&u)

	result.User.FirstName = u.FirstName
	result.User.LastName = u.LastName
	result.User.Mail = u.Mail
	result.User.Username = u.Username
	result.User.ID = u.ID
	result.Result = 0

	return result
}

func dbUserCreateToken(loginData UserLoginData) CreateUserResult {
	var result CreateUserResult
	var u User
	u.FirstName = "Uninitialized"
	dbOrm.Where("username = ? OR mail = ?", loginData.Name, loginData.Name).First(&u)

	if u.FirstName == "Uninitialized" {
		fmt.Println("Invalid login for", loginData.Name)
		result.Result = -1
		result.Message = ("Invalid login")
		return result
	}

	pwdMath := comparePasswords(u.Password, []byte(loginData.ClearPWD))
	if pwdMath == false {
		log.Println(err, u.Password, loginData.ClearPWD)
		result.Result = -1
		result.Message = ("Invalid login")
		return result
	}

	t := time.Now()

	hash, err := bcrypt.GenerateFromPassword([]byte(loginData.Name+t.String()), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
	}

	newToken := APIToken{Token: string(hash)}
	u.APITokens = append(u.APITokens, newToken)

	dbOrm.Save(&u)

	result.Result = 0
	result.Message = newToken.Token
	result.User.FirstName = u.FirstName
	result.User.LastName = u.LastName
	result.User.Mail = u.Mail
	result.User.Username = u.Username
	result.User.ID = u.ID
	return result
}

func dbUserConfirmToken(httpAuth string) bool {
	var u User
	var auth UserAuth
	var idx int

	if idx = strings.Index(httpAuth, ":"); idx < 0 {
		fmt.Println("User not found!")
		return false
	}

	auth.Username = httpAuth[0:idx]
	auth.Token = httpAuth[idx+1:]

	userNotFound := dbOrm.Where("username = ?", auth.Username).Preload("APITokens").Find(&u).RecordNotFound()

	if userNotFound {
		fmt.Println("User not found!", auth.Username)
		return false
	}

	for _, b := range u.APITokens {
		fmt.Println("Compare", b.Token, auth.Token)
		if b.Token == auth.Token {
			//	fmt.Println("User is valid!")
			return true
		}
	}

	//	fmt.Println("User is invalid!")
	return false
}

/*
func dbUserConfirmToken(auth UserAuth) bool {
	var u User

	userNotFound := dbOrm.Where("username = ?", auth.Username).Preload("APITokens").Find(&u).RecordNotFound()

	if userNotFound {
		fmt.Println("User not found!")
		return false
	}

	for _, b := range u.APITokens {
		//	fmt.Println("Compare", b.Token, auth.Token)
		if b.Token == auth.Token {
			//	fmt.Println("User is valid!")
			return true
		}
	}

	//	fmt.Println("User is invalid!")
	return false
}
*/
func comparePasswords(hashedPwd []byte, plainPwd []byte) bool {
	// Since we'll be getting the hashed password from the DB it
	// will be a string so we'll need to convert it to a byte slice
	//byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(hashedPwd, plainPwd)
	if err != nil {
		log.Println(err)
		return false
	}

	return true
}

func dbUserGetAll() Users {
	var list Users
	dbOrm.Find(&list)
	return list
}

func dbUserNameExists(username string) bool {
	var list Users

	dbOrm.Where("username = ?", username).Find(&list)
	if len(list) > 0 {
		return true
	} else {
		return false
	}
}

func dbMailaddressExists(username string) bool {
	var list Users

	dbOrm.Where("mail = ?", username).Find(&list)
	if len(list) > 0 {
		return true
	} else {
		return false
	}
}
