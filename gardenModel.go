package main

import (
	"time"

	"github.com/jinzhu/gorm"
)

/* API-Models*/
type apiGardenCreate struct {
	UserAuth
	Name string
}

type apiGardenCreateResult struct {
	apiGardenPublic
	Response
}
type apiGardenPublic struct {
	gorm.Model
	Name    string        `json:"name"`
	Border  []Coordinates `json:"border" gorm:"foreignkey:Ref"`
	Patches []Patch       `json:"patches" gorm:"foreignkey:Parent"`
}

/* Database-Models*/

type Garden struct {
	apiGardenPublic
	Owner uint `json:"user"`
}

type Patch struct {
	gorm.Model
	Name    string        `json:"name"`
	Border  []Coordinates `json:"border" gorm:"foreignkey:Ref"`
	History []Planting    `json:"history" gorm:"foreignkey:Ref"`
	Parent  uint          `json:"parent"`
}

type Coordinates struct {
	gorm.Model
	Longitude float32 `json:"long"`
	Latitude  float32 `json:"lat"`
	Order     int     `json:"order"`
	Ref       uint    `json:"ref"`
}

type Planting struct {
	gorm.Model
	Name     string  `json:"name"`
	Planted  Plant   `json:"planted"`
	Events   []Event `json:"events" gorm:"foreignkey:Ref`
	PatchRef uint    `json:"ref"`
}

type Event struct {
	gorm.Model
	Desc string    `json:"desc"`
	Date time.Time `json:"date"`
	Ref  uint      `json:"ref"`
}

type Plant struct {
	gorm.Model
	Name             string `json:"name"`
	Description      string
	SeedFrom         time.Time
	SeedTo           time.Time
	HarvestFrom      time.Time
	HarvestTo        time.Time
	GrowDurationMin  time.Time
	GrowDurationMMax time.Time
	RowDistance      int
	ColDistance      int
}
