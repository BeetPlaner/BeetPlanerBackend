package main

import (
	"github.com/jinzhu/gorm"
)

type APIToken struct {
	gorm.Model
	Token     string
	UserRefer uint
}

type UserAuth struct {
	Username string `json:"name"`
	Token    string
}

type User struct {
	gorm.Model
	CreateUserData
	APITokens []APIToken `json:"-" gorm:"foreignkey:UserRefer"`
	Password  []byte     `json:"password"`
	Gardens   []Garden   `json:"garden" gorm:"foreignkey:Owner"`
}

type UserPublicData struct {
	ID        uint   `json:"id"`
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
	Mail      string `json:"mail"`
	Username  string `json:"username"`
}

type CreateUserData struct {
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
	Mail      string `json:"mail"`
	Username  string `json:"username"`
	ClearPWD  string `json:"ClearPWD"`
}

type CreateUserResult struct {
	Result  int            `json:"result"`
	Message string         `json:"message"`
	User    UserPublicData `json:"user"`
}

type UserLoginData struct {
	Name     string `json:"name"`
	ClearPWD string `json:"password"`
}

type Response struct {
	Code    int
	Message string
}

type Users []User
