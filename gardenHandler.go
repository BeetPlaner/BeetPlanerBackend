package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

func GardenCreate(w http.ResponseWriter, r *http.Request) {
	var newGarden apiGardenCreate

	r.ParseForm()

	EscapeParameter(&r.Form)
	CheckParameter(&r.Form)

	tkn := r.Header.Get("API_TOKEN")

	if err != nil {
		panic(err)
	}

	//	fmt.Println("UserAuth will be checked!")
	if !dbUserConfirmToken(tkn) {
		fmt.Println("UserAuth not valid!")
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(403)
		return
	}

	t := dbGardenCreate(newGarden)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(t); err != nil {
		panic(err)
	}
}

func EscapeParameter(data *url.Values) {

	/*	for _, b := range data {
		fmt.Println("Compare", b.Token, auth.Token)
		if b.Token == auth.Token {
			//	fmt.Println("User is valid!")
			return true
		}
	}*/
}

func CheckParameter(data *url.Values) {

	/*	for _, b := range data {
		fmt.Println("Compare", b.Token, auth.Token)
		if b.Token == auth.Token {
			//	fmt.Println("User is valid!")
			return true
		}
	}*/
}

func PlantsList(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode(dbPlantsGetAll()); err != nil {
		panic(err)
	}
}
